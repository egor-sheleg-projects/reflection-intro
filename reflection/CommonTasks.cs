﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;


namespace Reflection.Tasks
{
    public static class CommonTasks
    {

        /// <summary>
        /// Returns the lists of public and obsolete classes for specified assembly.
        /// Please take attention: classes (not interfaces, not structs)
        /// </summary>
        /// <param name="assemblyName">name of assembly</param>
        /// <returns>List of public but obsolete classes</returns>
        public static IEnumerable<string> GetPublicObsoleteClasses(string assemblyName) 
        {
            return Assembly.ReflectionOnlyLoad(assemblyName).GetTypes()
                .Where(x => x.IsClass &&
                            x.IsPublic &&
                            Attribute.GetCustomAttributes(x)
                                .Any(y => y is ObsoleteAttribute))
                .Select(x => x.Name);
        }

        /// <summary>
        /// Returns the value for required property path
        /// </summary>
        /// <example>
        ///  1) 
        ///  string value = instance.GetPropertyValue("Property1")
        ///  The result should be equal to invoking statically
        ///  string value = instance.Property1;
        ///  2) 
        ///  string name = instance.GetPropertyValue("Property1.Property2.FirstName")
        ///  The result should be equal to invoking statically
        ///  string name = instance.Property1.Property2.FirstName;
        /// </example>
        /// <typeparam name="T">property type</typeparam>
        /// <param name="obj">source object to get property from</param>
        /// <param name="propertyPath">dot-separated property path</param>
        /// <returns>property value of obj for required propertyPath</returns>
        public static T GetPropertyValue<T>(this object obj, string propertyPath)
        {
            Type currentType = obj.GetType();

            foreach (string propertyName in propertyPath.Split('.'))
            {
                PropertyInfo property = currentType.GetProperty(propertyName);
                obj = property.GetValue(obj, null);
                currentType = property.PropertyType;
            }

            return (T)obj;
        }


        /// <summary>
        /// Assign the value to the required property path
        /// </summary>
        /// <example>
        ///  1)
        ///  instance.SetPropertyValue("Property1", value);
        ///  The result should be equal to invoking statically
        ///  instance.Property1 = value;
        ///  2)
        ///  instance.SetPropertyValue("Property1.Property2.FirstName", value);
        ///  The result should be equal to invoking statically
        ///  instance.Property1.Property2.FirstName = value;
        /// </example>
        /// <param name="obj">source object to set property to</param>
        /// <param name="propertyPath">dot-separated property path</param>
        /// <param name="value">assigned value</param>
        public static void SetPropertyValue(this object obj, string propertyPath, object value)
        {
            var parts = propertyPath.Split('.');
            var prop = obj.GetType().GetProperty(parts[0]);
            if (parts.Length == 1)
            {
                if (prop.CanWrite)
                {
                    prop.SetValue(obj, value, null);
                }
                else
                {
                    var field = obj.GetType().BaseType.GetField("<" + parts[0] + ">k__BackingField", 
                        BindingFlags.NonPublic | BindingFlags.Instance);
                    field.SetValue(obj, value);
                }
            }
            else
            {
                var set = prop.GetValue(obj);
                SetPropertyValue(set, string.Join(".", parts.Skip(1)), value);
            }
        }
    }
}
