﻿using System;
using System.Diagnostics;
using System.Linq;
 
using NUnit.Framework;
using Reflection.Tasks;

namespace Reflection.Tests
{
     
    public class CommonTests
    {
        
        #region Test Classes Declaration
        private class Employee {
            public string FirstName { get; set; }
            public string LastName  { get; private set; }

            public Employee(string firstName, string lastName)  {
                this.FirstName = firstName;
                this.LastName = lastName;
            }
        }

        private class Manager : Employee {
            public Manager(string firstName, string lastName) :  base(firstName, lastName) {}
        }

        private class Worker : Employee
        {
            public Manager Manager { get; private set; }
            public Worker(string firstName, string lastName, Manager manager) : base(firstName, lastName)
            {
                this.Manager = manager;
            }
        }
        #endregion Test Classes Declaration

        #region GetProperty tests
        [Test]
        public void GetPropertyShouldReturnPropertyValueForSinglePath()
        {
            var manager = new Manager("Joe", "Smith");
            var expected = manager.FirstName;
            var actual = manager.GetPropertyValue<string>("FirstName");
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPropertyShouldReturnPropertyValueForComplexPath()
        {
            var manager = new Manager("Joe", "Smith");
            var worker = new Worker("Willy", "Brown", manager);
            var expected = worker.Manager.FirstName;
            var actual = worker.GetPropertyValue<string>("Manager.FirstName");
            Assert.AreEqual(expected, actual);
        }
        #endregion GetProperty tests

        #region SetProperty tests
        [Test]
        public void SetPropertyShouldAssignValueForSinglePublicPath()
        {
            var manager = new Manager("Joe", "Smith");
            var expected = "Alex";

            manager.SetPropertyValue("FirstName", expected);
            
            var actual = manager.FirstName;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetPropertyShouldAssignValueForComplexPath()
        {
            var manager = new Manager("Joe", "Smith");
            var worker = new Worker("Willy", "Brown", manager);
            var expected = "Alex";

            worker.SetPropertyValue("Manager.FirstName", expected);

            var actual = worker.Manager.FirstName;
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void SetPropertyShouldAssignValueForSinglePrivatePath()
        {
            var manager = new Manager("Joe", "Smith");
            var expected = "Johnson";

            manager.SetPropertyValue("LastName", expected);

            var actual = manager.LastName;
            Assert.AreEqual(expected, actual);
        }
        #endregion SetProperty tests
    }
}
