## Task Description ##
**Reflection and run time code generation**


1. **Common tasks**:

After finishing this task, you will gain practical experience of working with the Reflection types in .NET.

To complete the task, you need to implement 3 methods that use Reflection to get information about assembly types and class members. 

It’s highly recommended to read the summary comment for every method, check the corresponding unit tests in <CommonTests>, and implement methods accordingly. 

**Run all unit tests with Visual Studio and make sure there are no failed unit tests. Fix your code to make all tests GREEN**. 

 
2. **CodeGeneration task**:

This task will give you a practical insight into runtime code generation with expression trees and demonstrate how to apply various runtime ORM frameworks. 


Before starting to work on the task, please check out this [article](https://devblogs.microsoft.com/visualstudio/generating-dynamic-methods-with-expression-trees-in-visual-studio-2010/). Even though the material refers to VS 2010, the provided sample is relevant for VS 2019 as well. 

 
To complete the task, you need to implement **a single method** that returns a function that can produce multiplication of two arrays of integers:  

_Func<T[], T[], T> GetVectorMultiplyFunction<T>()_  

Explanation: 

Functions should return the scalar product of vectors:  

/// (a1, a2,...,aN) * (b1, b2, ..., bN) = a1*b1 + a2*b2 + ... + aN*bN  
 
**Make sure that all unit tests GREEN before submitting the task for an automatic check.** 


 
